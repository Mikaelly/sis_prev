<%@ include file="../body_top.jsp"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"></h1>
		<form role="form" action="<c:url value='/artefato/insert'/>" method="post">
			<div class="form-group">
				<c:forEach var="error" items="${errors}">
					<div class="alert alert-danger" role="alert">${error.message}</div>
				</c:forEach>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">Cadastro Artefato</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Nome</label> <input class="form-control" name="artefato.nome" id="nome" type="text" value="${artefato.nome}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Quantidade</label> <input class="form-control" name="artefato.quantidade" id="quantidade" type="text" value="${artefato.quantidade}">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group ">
								<label>Valor Kw/hr</label>
								<div class="form-group input-group">
									<span class="input-group-addon">$</span> 
									<input type="text" name="artefato.valorKwHr" id="valorKwHr" class="form-control" value="${artefato.valorKwHr}"> 
									<span class="input-group-addon">.00</span>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Empresa</label>
								<select class="form-control" name="artefato.idEmpresa" value="${artefato.idEmpresa}">
									<option>Selecione</option>
									<c:forEach var="empresa" items="${empresas}">
										<option value="${empresa.empresa}">${empresa.nome}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-success" type="submit">
						<i class="glyphicon glyphicon-ok"></i> Salvar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Listagem</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Nome</th>
								<th>Quantidade</th>
								<th>Valor Kw/hr</th>
								<th>Empresa</th>
								<th>A��o</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="artefato" items="${artefatos}">
								<tr>
									<td>${artefato.artefato}</td>
									<td style="width: 50%;">${artefato.nome}</td>
									<td>${artefato.quantidade}</td>
									<td>${artefato.valorKwHr}</td>
								    <td>${artefato.empresa}</td>
									<td>
										<a href="<c:url value='/artefato/delete/${artefato.artefato}'/>">
											<button type="button" class="btn btn-outline btn-danger btn-circle" title="Excluir">
												<i class="fa fa-times"></i>
											</button>
										</a>
										<a href="<c:url value='/artefato/alterar/${artefato.artefato}'/>">
											<button type="button" class="btn btn-outline btn-warning btn-circle" title="Alterar">
												<i class="fa fa-pencil"></i>
											</button>
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="../body_final.jsp"%>
