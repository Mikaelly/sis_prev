<%@ include file="../body_top.jsp"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"></h1>
		<form role="form" action="<c:url value='/meta/update/${meta.meta}'/>" method="post">
			<div class="form-group">
				<c:forEach var="error" items="${errors}">
					<div class="alert alert-danger" role="alert">${error.message}</div>
				</c:forEach>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">Alterar Meta</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Descri��o</label>
								<input class="form-control" name="meta.descricao" type="text" value="${meta.descricao}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<label>Data Inicio</label>
								<input class="form-control" name="meta.dataInicio" type="text" value="${meta.dataInicio}">
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Data Fim</label>
								<input class="form-control" name="meta.dataFim"  type="text" value="${meta.dataFim}">
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Empresa</label>
								<select class="form-control" name="meta.empresa">
									<option>Selecione</option>
									<c:forEach var="empresa" items="${empresas}">
										<option value="${empresa.empresa}"  ${empresa.empresa eq meta.empresa ? "selected='selected'" : ""}>${empresa.nome}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group ">
								<label>Valor Meta</label>
								<div class="form-group input-group">
									<span class="input-group-addon">$</span>
									<input type="text" name="meta.valorMeta" class="form-control" value="${meta.valorMeta}">
									<span class="input-group-addon">.00</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-default" type="submit">
						<i class="glyphicon glyphicon-ok"></i>
						Salvar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="../body_final.jsp"%>
