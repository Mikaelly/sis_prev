<%@ include file="../body_top.jsp"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"></h1>
		<form role="form" action="<c:url value='/meta/insert'/>" method="post">
			<div class="form-group">
				<c:forEach var="error" items="${errors}">
					<div class="alert alert-danger" role="alert">${error.message}</div>
				</c:forEach>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">Cadastro Meta</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Descri��o</label>
								<input class="form-control" name="meta.descricao" type="text" value="${meta.descricao}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<label>Data Inicio</label>
								<input class="form-control" name="meta.dataInicio" type="text" value="${meta.dataInicio}">
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Data Fim</label>
								<input class="form-control" name="meta.dataFim"  type="text" value="${meta.dataFim}">
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Empresa</label>
								<select class="form-control" name="meta.empresa" value="${meta.empresa}">
									<option>Selecione</option>
									<c:forEach var="empresa" items="${empresas}">
										<option value="${empresa.empresa}">${empresa.nome}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group ">
								<label>Valor Meta</label>
								<div class="form-group input-group">
									<span class="input-group-addon">$</span>
									<input type="text" name="meta.valorMeta" class="form-control" value="${meta.valorMeta}">
									<span class="input-group-addon">.00</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-default" type="submit">
						<i class="glyphicon glyphicon-ok"></i>
						Salvar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Listagem</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Descri��o</th>
								<th>Data Inicio</th>
								<th>Data Fim</th>
								<th>Valor Meta</th>
								<th>Empresa</th>
								<th>A��o</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="meta" items="${metaList}">
								<tr>
									<td>${meta.meta}</td>
									<td>${meta.descricao}</td>
									<td>${meta.dataInicio}</td>
									<td>${meta.dataFim}</td>
									<td>${meta.valorMeta}</td>
									<td>${meta.empresaNome}</td>
									<td>
										<a href="<c:url value='/meta/delete/${meta.meta}'/>">
											<button type="button" class="btn btn-outline btn-danger btn-circle" title="Excluir">
												<i class="fa fa-times"></i>
											</button>
										</a>
										<a href="<c:url value='/meta/alterar/${meta.meta}'/>">
											<button type="button" class="btn btn-outline btn-warning btn-circle" title="Alterar">
												<i class="fa fa-pencil"></i>
											</button>
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="../body_final.jsp"%>
