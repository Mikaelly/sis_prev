<%@ include file="../body_top.jsp"%>
<div class="row">
	<h1 class="page-header"></h1>
	<form role="form" action="<c:url value='/simulacao/calculo'/>" method="post">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Simulando</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Metas</label>
								<select class="form-control" name="simulacao.meta">
									<c:forEach var="metas" items="${metas}">
										<option value="${metas.meta}">${metas.descricao}</option>
									</c:forEach>							
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Artefatos</label>
								<select multiple class="form-control" name="simulacao.artefato">
									<c:forEach var="artefatos" items="${artefatos}">
										<option value="${artefatos.artefato}">${artefatos.nome}</option>
									</c:forEach>							
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary" type="submit">
						<span class="fa fa-cog"></span> 
						Simular
					</button>
				</div>
			</div>
		</div>
	</form>
</div>

<div>

	Desligar todo dia essa quantidade por artefato para bater meta ------ ${artefato}
</div>
<%@ include file="../body_final.jsp"%>