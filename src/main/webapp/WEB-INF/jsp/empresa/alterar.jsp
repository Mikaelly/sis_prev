<%@ include file="../body_top.jsp"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"></h1>
		<form role="form" action="<c:url value='/empresa/update/${empresa.empresa}'/>" method="post">
			<div class="form-group">
				<c:forEach var="error" items="${errors}">
					<div class="alert alert-danger" role="alert">${error.message}</div>
				</c:forEach>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">Edita Empresa</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Nome</label>
								<input class="form-control" name="empresa.nome" type="text" value="${empresa.nome}">
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary" type="submit">
						<i class="glyphicon glyphicon-ok"></i>
						Salvar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="../body_final.jsp"%>