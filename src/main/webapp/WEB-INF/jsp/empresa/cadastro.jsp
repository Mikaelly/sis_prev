<%@ include file="../body_top.jsp"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"></h1>
		<form role="form" action="<c:url value='/empresa/insert'/>" method="post">
			<div class="form-group">
				<c:forEach var="error" items="${errors}">
					<div class="alert alert-danger" role="alert">${error.message}</div>
				</c:forEach>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">Cadastro Empresa</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Nome</label>
								<input class="form-control" name="empresa.nome" type="text" value="${empresa.nome}">
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary" type="submit">
						<i class="glyphicon glyphicon-ok"></i>
						Salvar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Listagem</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Nome</th>
								<th>A��o</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="empresa" items="${empresaList}">
								<tr>
									<td>${empresa.empresa}</td>
									<td style="width: 85%;">${empresa.nome}</td>
									<td>
										<a href="<c:url value='/empresa/delete/${empresa.empresa}'/>">
											<button type="button" class="btn btn-outline btn-danger btn-circle" title="Excluir">
												<i class="fa fa-times"></i>
											</button>
										</a>
										<a href="<c:url value='/empresa/alterar/${empresa.empresa}'/>">
											<button type="button" class="btn btn-outline btn-warning btn-circle" title="Alterar">
												<i class="fa fa-pencil"></i>
											</button>
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="../body_final.jsp"%>
