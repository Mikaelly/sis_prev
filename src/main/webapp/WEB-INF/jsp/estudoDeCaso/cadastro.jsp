<%@ include file="../body_top.jsp"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"></h1>
		<form role="form" action="<c:url value='/estudoDeCaso/insert'/>" method="post">
			<div class="panel panel-default">
				<div class="panel-heading">Cadastro Estudo de Caso</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Descri��o</label> 
								<input class="form-control" name="estudoDeCaso.descricao" type="text" value="${estudoDeCaso.descricao}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Quantidade Hr/Diario</label> 
								<input class="form-control" name="estudoDeCaso.quantHrDiario" id="quantHrDiario" type="text" value="${estudoDeCaso.quantHrDiario }">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group ">
								<label>Total Hr/Mensal</label>
								<input class="form-control" name="estudoDeCaso.totalHrMensal" id="totalHrMensal" type="text" value="${estudoDeCaso.totalHrMensal }">								
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Artefato</label>
								<select class="form-control" name="estudoDeCaso.idArtefato" value="${estudoDeCaso.idArtefato}">
									<option>Selecione</option>
									<c:forEach var="artefato" items="${artefatos}">
										<option value="${artefato.artefato}">${artefato.nome}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Informa��es Adicionais</label> 
								<textarea class="form-control" name="estudoDeCaso.informacaoAdicional" type="text" value="${estudoDeCaso.informacaoAdicional}"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-success" type="submit">
						<i class="glyphicon glyphicon-ok"></i> Salvar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Listagem</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Descri��o</th>
								<th>Quantidade Hr/di�rio</th>
								<th>Total Hr/mensal</th>
								<th>Artefato</th>
								<th>A��o</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="estudoDeCaso" items="${estudoDeCasoList}">
								<tr>
									<td>${estudoDeCaso.estudoDeCaso}</td>
									<td style="width: 35%;">${estudoDeCaso.descricao}</td>
									<td>${estudoDeCaso.quantHrDiario}</td>
									<td>${estudoDeCaso.totalHrMensal}</td>
									<td>${estudoDeCaso.nomeArtefato}</td>
									<td>
										<a href="<c:url value='/estudoDeCaso/delete/${estudoDeCaso.estudoDeCaso}'/>">
											<button type="button" class="btn btn-outline btn-danger btn-circle" title="Excluir">
												<i class="fa fa-times"></i>
											</button>
										</a>
										<a href="<c:url value='/estudoDeCaso/alterar/${estudoDeCaso.estudoDeCaso}'/>">
											<button type="button" class="btn btn-outline btn-warning btn-circle" title="Alterar">
												<i class="fa fa-pencil"></i>
											</button>
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="../body_final.jsp"%>
