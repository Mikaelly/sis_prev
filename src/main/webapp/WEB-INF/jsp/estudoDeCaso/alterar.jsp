<%@ include file="../body_top.jsp"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"></h1>
		<form role="form" action="<c:url value='/estudoDeCaso/update/${estudoDeCaso.estudoDeCaso}'/>" method="post">
			<div class="panel panel-default">
				<div class="panel-heading">Alterar Estudo de Caso</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Descrição</label> 
								<input class="form-control" name="estudoDeCaso.descricao" type="text" value="${estudoDeCaso.descricao}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Quantidade Hr/Diario</label> 
								<input class="form-control" name="estudoDeCaso.quantHrDiario" id="quantHrDiario" type="text" value="${estudoDeCaso.quantHrDiario }">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group ">
								<label>Total Hr/Mensal</label>
								<input class="form-control" name="estudoDeCaso.totalHrMensal" id="totalHrMensal" type="text" value="${estudoDeCaso.totalHrMensal }">								
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Artefato</label>
								<select class="form-control" name="estudoDeCaso.idArtefato" value="${estudoDeCaso.idArtefato}">
									<option>Selecione</option>
									<c:forEach var="artefato" items="${artefatos}">
										<option value="${artefato.artefato}" ${artefato.artefato eq estudoDeCaso.idArtefato ? "selected='selected'" : ""}>${artefato.nome}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Informações Adicionais</label> 
								<textarea class="form-control" name="estudoDeCaso.informacaoAdicional" type="text">${estudoDeCaso.informacaoAdicional}</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-success" type="submit">
						<i class="glyphicon glyphicon-ok"></i> Salvar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="../body_final.jsp"%>