<%@ include file="../head.jsp"%>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Please Sign In</h3>
					</div>
					<div class="panel-body">
						<form role="form" action="<c:url value='/autentica'/>" method="post">
							<fieldset>
								<div class="form-group">
									<c:forEach var="error" items="${errors}">
										<div class="alert alert-danger" role="alert">${error.message}</div>
									</c:forEach>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="E-mail" name="login.login" id="login" type="text" autofocus>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Password" name="login.senha" id="senha" type="password" value="">
								</div>
								<div class="checkbox">
									<label>
										<input name="remember" type="checkbox" value="Remember Me">
										Remember Me
									</label>
								</div>
								<!-- Change this to a button or input when using this as a form -->
								<button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="../script.jsp"%>
</body>
</html>