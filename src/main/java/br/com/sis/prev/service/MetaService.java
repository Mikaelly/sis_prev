package br.com.sis.prev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.caelum.vraptor.ioc.Component;
import br.com.sis.prev.model.Meta;
import br.com.sis.prev.persistence.MetaMapper;

@Component
@Transactional
public class MetaService {

	@Autowired
	private MetaMapper mapper;

	public List<Meta> select() {
		return mapper.select();
	}

	public void insert(Meta meta) {
		mapper.insert(meta);
	}
	
	public void delete(int meta) {
		mapper.delete(meta);
	}
	
	public Meta alterar(int meta) {
		return mapper.selectById(meta);
	}
	
	public void update(Meta meta) {
		mapper.update(meta);
	}
}
