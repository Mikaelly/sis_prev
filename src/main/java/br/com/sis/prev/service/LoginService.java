package br.com.sis.prev.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.caelum.vraptor.ioc.Component;
import br.com.sis.prev.model.Login;
import br.com.sis.prev.persistence.LoginMapper;

@Component
@Transactional
public class LoginService {
	
	@Autowired
	private LoginMapper mapper;
	
	public Login autentica(String login, String senha) {
		return mapper.autenticaLogin(login, senha);
	}
}
