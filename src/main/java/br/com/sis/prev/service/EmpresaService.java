package br.com.sis.prev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.caelum.vraptor.ioc.Component;
import br.com.sis.prev.model.Empresa;
import br.com.sis.prev.persistence.EmpresaMapper;

@Component
@Transactional
public class EmpresaService {

	@Autowired
	private EmpresaMapper mapper;

	public List<Empresa> select() {
		return mapper.select();
	}

	public void insert(Empresa empresa) {
		mapper.insert(empresa);
	}
	
	public void delete(int empresa) {
		mapper.delete(empresa);
	}
	
	public Empresa alterar(int empresa) {
		return mapper.selectById(empresa);
	}
	
	public void update(Empresa empresa) {
		mapper.update(empresa);
	}
}
