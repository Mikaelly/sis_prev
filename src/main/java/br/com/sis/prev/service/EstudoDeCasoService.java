package br.com.sis.prev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.caelum.vraptor.ioc.Component;
import br.com.sis.prev.model.EstudoDeCaso;
import br.com.sis.prev.persistence.EstudoDeCasoMapper;

@Component
@Transactional
public class EstudoDeCasoService {

	@Autowired
	private EstudoDeCasoMapper mapper;

	public List<EstudoDeCaso> select() {
		return mapper.select();
	}

	public void insert(EstudoDeCaso estudoDeCaso) {
		mapper.insert(estudoDeCaso);
	}
	
	public void delete(int estudoDeCaso) {
		mapper.delete(estudoDeCaso);
	}
	
	public EstudoDeCaso alterar(int estudoDeCaso) {
		return mapper.selectById(estudoDeCaso);
	}
	
	public void update(EstudoDeCaso estudoDeCaso) {
		mapper.update(estudoDeCaso);
	}
}
