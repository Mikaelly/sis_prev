package br.com.sis.prev.service;

import java.util.Calendar;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;
import br.com.sis.prev.model.Login;

@Component
@SessionScoped
public class UsuarioLogadoService {
	private Login login;
	private Calendar horaLogin;
	
	public void sessionLogin(Login login, Calendar horario) {
		this.login = login;
		this.horaLogin = horario;
	}
	
//	public void logount() {
//		this.login.removeAttribute();
//	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public Calendar getHoraLogin() {
		return horaLogin;
	}

	public void setHoraLogin(Calendar horaLogin) {
		this.horaLogin = horaLogin;
	}
	
	
}
