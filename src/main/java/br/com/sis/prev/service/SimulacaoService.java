package br.com.sis.prev.service;

import java.util.List;

import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.caelum.vraptor.ioc.Component;
import br.com.sis.prev.model.Artefato;
import br.com.sis.prev.model.Meta;
import br.com.sis.prev.model.Simulacao;
import br.com.sis.prev.persistence.SimulacaoMapper;

@Component
@Transactional
public class SimulacaoService {

	@Autowired
	private MetaService mapper;
	@Autowired
	private ArtefatoService artefatoService;
	@Autowired
	private MetaService metaService;
	@Autowired
	private SimulacaoMapper simulacaoMapper;

	public List<Artefato> artefato() {
		return artefatoService.select();
	}

	public List<Meta> meta() {
		return metaService.select();
	}

	public Double calculo(Simulacao simulacao) {
		return artefatoAtributos(simulacao);
	}

	public Double artefatoAtributos(Simulacao simulacao) {
		Meta m = metaService.alterar(simulacao.getMeta());

		Integer valorMeta = m.getValorMeta();
		Integer diaObterMeta = Days.daysBetween(m.getDataInicio(),m.getDataFim()).getDays();

		Double valorTotalDiario_ECONOMIZAR = 0.0;
		Double quantMinutosEconomizar = 0.0;
			
		for (Integer artefato : simulacao.getArtefato()) {
			Artefato a = artefatoService.alterar(artefato);
			Simulacao s = simulacaoMapper.selectQuantHrDiarioArtefato(artefato);
			
			valorTotalDiario_ECONOMIZAR = (((((s.getQuantHrDiario() * diaObterMeta) * a.getValorKwHr()) * a.getQuantidade()) - valorMeta) / a.getValorKwHr()) / diaObterMeta ;
		
			quantMinutosEconomizar = (s.getQuantHrDiario() * a.getQuantidade())- valorTotalDiario_ECONOMIZAR;
		}
		
		return quantMinutosEconomizar;

	}

}
