package br.com.sis.prev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.caelum.vraptor.ioc.Component;
import br.com.sis.prev.model.Artefato;
import br.com.sis.prev.persistence.ArtefatoMapper;

@Component
@Transactional
public class ArtefatoService {

	@Autowired
	private ArtefatoMapper mapper;

	public List<Artefato> select() {
		return mapper.select();
	}

	public void insert(Artefato artefato) {
		mapper.insert(artefato);
	}
	
	public void delete(int artefato) {
		mapper.delete(artefato);
	}
	
	public Artefato alterar(int artefato) {
		return mapper.selectById(artefato);
	}
	
	public void update(Artefato artefato) {
		mapper.update(artefato);
	}
}
