package br.com.sis.prev.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.sis.prev.model.Meta;

public interface MetaMapper {

	public List<Meta> select();

	public void insert(Meta meta);
	
	public void delete(@Param("meta") int meta);
	
	public Meta selectById(@Param("meta") int meta);
	
	public void update(Meta meta);

}
