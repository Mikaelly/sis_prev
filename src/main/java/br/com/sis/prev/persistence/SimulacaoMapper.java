package br.com.sis.prev.persistence;

import org.apache.ibatis.annotations.Param;

import br.com.sis.prev.model.Simulacao;

public interface SimulacaoMapper {
	
	public Simulacao selectQuantHrDiarioArtefato(@Param("artefato") Integer artefato);
}
