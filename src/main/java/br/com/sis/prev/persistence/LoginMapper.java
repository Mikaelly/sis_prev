package br.com.sis.prev.persistence;

import org.apache.ibatis.annotations.Param;

import br.com.sis.prev.model.Login;

public interface LoginMapper {
	public Login autenticaLogin(@Param("login") String login,@Param("senha") String senha);
}
