package br.com.sis.prev.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.sis.prev.model.Artefato;


public interface ArtefatoMapper {

	public List<Artefato> select();

	public void insert(Artefato artefato);
	
	public void delete(@Param("artefato") int artefato);
	
	public Artefato selectById(@Param("artefato") int artefato);
	
	public void update(Artefato artefato);

}
