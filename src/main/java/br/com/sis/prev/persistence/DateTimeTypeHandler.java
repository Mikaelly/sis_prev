package br.com.sis.prev.persistence;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

@MappedTypes(LocalDate.class)
public class DateTimeTypeHandler implements TypeHandler<Object> {

	@Override
	public Object getResult(ResultSet rs, String columnName)
			throws SQLException {
		Timestamp ts = rs.getTimestamp(columnName);
		if (ts != null) {
			return new LocalDate(ts.getTime(), DateTimeZone.UTC);
		} else {
			return null;
		}
	}

	@Override
	public Object getResult(CallableStatement arg0, int arg1)
			throws SQLException {
		return null;
	}

	@Override
	public void setParameter(PreparedStatement ps, int i, Object parameter,
			JdbcType jdbcType) throws SQLException {
		if (parameter != null) {
			Date date = new Date(((LocalDate) parameter).toDate().getTime());
			ps.setDate(i, date);
		} else {
			ps.setDate(i, null);
		}
	}

	public Object getResult(ResultSet arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}