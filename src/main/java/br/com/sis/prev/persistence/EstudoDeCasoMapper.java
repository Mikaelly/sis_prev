package br.com.sis.prev.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.sis.prev.model.EstudoDeCaso;;


public interface EstudoDeCasoMapper {

	public List<EstudoDeCaso> select();

	public void insert(EstudoDeCaso estudoDeCaso);
	
	public void delete(@Param("estudoDeCaso") int estudoDeCaso);
	
	public EstudoDeCaso selectById(@Param("estudoDeCaso") int estudoDeCaso);
	
	public void update(EstudoDeCaso estudoDeCaso);

}
