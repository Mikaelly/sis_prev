package br.com.sis.prev.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.sis.prev.model.Empresa;

public interface EmpresaMapper {

	public List<Empresa> select();

	public void insert(Empresa empresa);
	
	public void delete(@Param("empresa") int empresa);
	
	public Empresa selectById(@Param("empresa") int empresa);
	
	public void update(Empresa empresa);

}
