package br.com.sis.prev.model;

import org.joda.time.LocalDate;

public class Meta {
	private Integer meta;
	private String descricao;
	private Integer valorMeta;
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private Integer empresa;
	private String empresaNome;

	public String getEmpresaNome() {
		return empresaNome;
	}

	public void setEmpresaNome(String empresaNome) {
		this.empresaNome = empresaNome;
	}

	public Integer getMeta() {
		return meta;
	}

	public void setMeta(Integer meta) {
		this.meta = meta;
	}

	public int getValorMeta() {
		return valorMeta;
	}

	public void setValorMeta(int valorMeta) {
		this.valorMeta = valorMeta;
	}

	public LocalDate getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDate getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}

	public void setValorMeta(Integer valorMeta) {
		this.valorMeta = valorMeta;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
