package br.com.sis.prev.model;

public class EstudoDeCaso {
	private Integer estudoDeCaso;
	private String descricao;
	private double quantHrDiario;
	private double totalHrMensal;
	private String informacaoAdicional;
	private String nomeArtefato;
	private Integer idArtefato;

	public Integer getEstudoDeCaso() {
		return estudoDeCaso;
	}

	public void setEstudoDeCaso(Integer estudoDeCaso) {
		this.estudoDeCaso = estudoDeCaso;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getQuantHrDiario() {
		return quantHrDiario;
	}

	public void setQuantHrDiario(double quantHrDiario) {
		this.quantHrDiario = quantHrDiario;
	}

	public double getTotalHrMensal() {
		return totalHrMensal;
	}

	public void setTotalHrMensal(double totalHrMensal) {
		this.totalHrMensal = totalHrMensal;
	}

	public String getInformacaoAdicional() {
		return informacaoAdicional;
	}

	public void setInformacaoAdicional(String informacaoAdicional) {
		this.informacaoAdicional = informacaoAdicional;
	}

	public String getNomeArtefato() {
		return nomeArtefato;
	}

	public void setNomeArtefato(String nomeArtefato) {
		this.nomeArtefato = nomeArtefato;
	}

	public Integer getIdArtefato() {
		return idArtefato;
	}

	public void setIdArtefato(Integer idArtefato) {
		this.idArtefato = idArtefato;
	}

}
