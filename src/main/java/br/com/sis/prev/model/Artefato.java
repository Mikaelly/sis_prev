package br.com.sis.prev.model;

public class Artefato {
	private Integer artefato;
	private String nome;
	private Integer quantidade;
	private Double valorKwHr;
	private String empresa;
	private Integer idEmpresa;

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Integer getArtefato() {
		return artefato;
	}

	public void setArtefato(Integer artefato) {
		this.artefato = artefato;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	
	public Double getValorKwHr() {
		return valorKwHr;
	}

	public void setValorKwHr(Double valorKwHr) {
		this.valorKwHr = valorKwHr;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

}
