package br.com.sis.prev.model;

import java.util.List;

public class Simulacao {
	private List<Integer> artefato;
	private Integer meta;
	private Integer quantHrDiario;

	public List<Integer> getArtefato() {
		return artefato;
	}

	public void setArtefato(List<Integer> artefato) {
		this.artefato = artefato;
	}

	public Integer getMeta() {
		return meta;
	}

	public void setMeta(Integer meta) {
		this.meta = meta;
	}

	public Integer getQuantHrDiario() {
		return quantHrDiario;
	}

	public void setQuantHrDiario(Integer quantHrDiario) {
		this.quantHrDiario = quantHrDiario;
	}

}
