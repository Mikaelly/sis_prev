package br.com.sis.prev.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.sis.prev.model.Empresa;
import br.com.sis.prev.service.EmpresaService;
import br.com.sis.prev.service.UsuarioLogadoService;

@Path("/empresa")
@Resource
public class EmpresaController {

	private final Result result;
	private Validator validator;

	@Autowired
	private UsuarioLogadoService usuarioLogado;

	@Autowired
	private EmpresaService dao;

	public EmpresaController(Result result, Validator validator) {
		this.result = result;
		this.validator = validator;
	}

	@Path("/cadastro")
	public List<Empresa> cadastro() {
		if (usuarioLogado.getLogin() == null) {
			result.redirectTo(LoginController.class).login();
		}

		return dao.select();		
	}

	@Post("/insert")
	public void insert(final Empresa empresa) {
		if (usuarioLogado.getLogin() != null) {
			validator.checking(new Validations() {
				{
					that(empresa.getNome() != null, "ERROR", "nome.nao.informado");
				}
			});

			validator.onErrorForwardTo(this).cadastro();

			dao.insert(empresa);
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
	
	@Path("/delete/{empresa.empresa}")
	public void delete(Empresa empresa) {
		if(usuarioLogado.getLogin() != null) {
			dao.delete(empresa.getEmpresa());
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}	
	}
	
	@Path("/alterar/{empresa.empresa}")
	public Empresa alterar(Empresa empresa) {
		return dao.alterar(empresa.getEmpresa());		
	}
	
	@Post
	@Path("/update/{empresa.empresa}")
	public void update(final Empresa empresa) {
		if (usuarioLogado.getLogin() != null) {
			validator.checking(new Validations() {
				{
					that(empresa.getNome() != null, "ERROR", "nome.nao.informado");
				}
			});

			validator.onErrorUsePageOf(this).alterar(empresa);

			dao.update(empresa);
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
}
