package br.com.sis.prev.controller;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.sis.prev.model.Meta;
import br.com.sis.prev.service.EmpresaService;
import br.com.sis.prev.service.MetaService;
import br.com.sis.prev.service.UsuarioLogadoService;

@Path("/meta")
@Resource
public class MetasController {
	
	private final Result result;
	private Validator validator;
	
	@Autowired
	private UsuarioLogadoService usuarioLogado;
	
	@Autowired
	private MetaService dao;
	
	@Autowired
	private EmpresaService empresadao;

	public MetasController(Result result, Validator validator) {
		this.result = result;
		this.validator = validator;
	}
	
	@Path("/cadastro")
	public void cadastro(){
		if(usuarioLogado.getLogin() == null) {
			result.redirectTo(LoginController.class).login();
		}
		result.include("empresas", empresadao.select());
		result.include("metaList", dao.select());

	}
	
	@Post()
	@Path("/insert")
	public void insert(final Meta meta) {
		if (usuarioLogado.getLogin() != null) {
			validator.checking(new Validations() {
				{
					that(meta.getDataFim() != null, "ERROR", "campos.nao.informado");
					that(meta.getDataInicio() != null, "ERROR", "campos.nao.informado");
					that(meta.getEmpresa() != 0, "ERROR", "campos.nao.informado");
					that(meta.getValorMeta() != 0 , "ERROR", "campos.nao.informado");
					that(meta.getDescricao() != null, "ERROR", "campos.nao.informado");
				}
			});

			validator.onErrorForwardTo(this).cadastro();

			dao.insert(meta);
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
	
	@Path("/delete/{meta.meta}")
	public void delete(Meta meta) {
		if(usuarioLogado.getLogin() != null) {
			dao.delete(meta.getMeta());
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}	
	}
	
	@Path("/alterar/{meta.meta}")
	public Meta alterar(Meta meta) {
		result.include("empresas", empresadao.select());
		return dao.alterar(meta.getMeta());		
	}
	
	@Post
	@Path("/update/{meta.meta}")
	public void update(final Meta meta) {
		if (usuarioLogado.getLogin() != null) {
			validator.checking(new Validations() {
				{
					that(meta.getDataFim() != null, "ERROR", "campos.nao.informado");
					that(meta.getDataInicio() != null, "ERROR", "campos.nao.informado");
					that(meta.getEmpresa() != 0, "ERROR", "campos.nao.informado");
					that(meta.getValorMeta() != 0 , "ERROR", "campos.nao.informado");
					that(meta.getDescricao() != null, "ERROR", "campos.nao.informado");
				}
			});

			validator.onErrorUsePageOf(this).alterar(meta);

			dao.update(meta);
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
}
