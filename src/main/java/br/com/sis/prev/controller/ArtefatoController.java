package br.com.sis.prev.controller;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.sis.prev.model.Artefato;
import br.com.sis.prev.service.ArtefatoService;
import br.com.sis.prev.service.EmpresaService;
import br.com.sis.prev.service.UsuarioLogadoService;

@Path("/artefato")
@Resource
public class ArtefatoController {
	
	private final Result result;
	private Validator validator;
	
	@Autowired
	private UsuarioLogadoService usuarioLogado;
	
	@Autowired
	private ArtefatoService dao;
	
	@Autowired
	private EmpresaService empresaDao;

	public ArtefatoController(Result result, Validator validator) {
		this.result = result;
		this.validator = validator;
	}
	
	@Path("/cadastro")
	public void cadastro(){
		if(usuarioLogado.getLogin() == null) {
			result.redirectTo(LoginController.class).login();
		}

		result.include("empresas", empresaDao.select());
		result.include("artefatos", dao.select());
	}
	
	@Post("/insert")
	public void insert(final Artefato artefato) {
		if (usuarioLogado.getLogin() != null) {
			validator.checking(new Validations() {
				{
					that(artefato.getNome() != null, "ERROR", "campos.nao.informado");
					that(artefato.getQuantidade() != 0, "ERROR", "campos.nao.informado");
					that(artefato.getValorKwHr() != 0, "ERROR", "campos.nao.informado");
				}
			});

			validator.onErrorForwardTo(this).cadastro();

			dao.insert(artefato);
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
	
	@Path("/delete/{artefato.artefato}")
	public void delete(Artefato artefato) {
		if(usuarioLogado.getLogin() != null) {
			dao.delete(artefato.getArtefato());
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}	
	}
	
	@Path("/alterar/{artefato.artefato}")
	public void alterar(Artefato artefato) {
		if(usuarioLogado.getLogin() != null) {
			result.include("empresas", empresaDao.select());
			result.include("artefato", dao.alterar(artefato.getArtefato()));	
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
		
	@Post
	@Path("/update/{artefato.artefato}")
	public void update(final Artefato artefato) {
		if (usuarioLogado.getLogin() != null) {
			validator.checking(new Validations() {
				{
					that(artefato.getNome() != null, "ERROR", "campos.nao.informado");
					that(artefato.getQuantidade() != 0, "ERROR", "campos.nao.informado");
					that(artefato.getValorKwHr() != 0, "ERROR", "campos.nao.informado");
				}
			});

			validator.onErrorUsePageOf(this).alterar(artefato);

			dao.update(artefato);
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
}
