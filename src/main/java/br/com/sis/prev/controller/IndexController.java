package br.com.sis.prev.controller;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.sis.prev.service.UsuarioLogadoService;

@Resource
public class IndexController {
	
	private final Result result;
	@Autowired
	private UsuarioLogadoService usuarioLogado;

	public IndexController(Result result) {
		this.result = result;
	}	

	public void index() {
		if(usuarioLogado.getLogin() == null) {
			result.redirectTo(LoginController.class).login();
		}
	}
	
	@Path("/logout")
	public void logout() {
		usuarioLogado.setLogin(null);
		usuarioLogado.setHoraLogin(null);
		
		result.redirectTo(LoginController.class).login();
	}
}
