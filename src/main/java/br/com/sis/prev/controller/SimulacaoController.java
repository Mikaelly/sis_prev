package br.com.sis.prev.controller;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.sis.prev.model.Simulacao;
import br.com.sis.prev.service.SimulacaoService;
import br.com.sis.prev.service.UsuarioLogadoService;

@Path("/simulacao")
@Resource
public class SimulacaoController {
	
	@Autowired
	private UsuarioLogadoService usuarioLogado;
	@Autowired
	private SimulacaoService service;
	
	private final Result result;
	
	public SimulacaoController(Result result) {
		this.result = result;
	}

	@Path("/form")
	@Get()
	public void form() {
		if(usuarioLogado.getLogin() == null) {
			result.redirectTo(LoginController.class).login();
		}
		
		result.include("artefatos", service.artefato());
		result.include("metas", service.meta());
	}
	
	public void calculo(Simulacao simulacao) {
		if(usuarioLogado.getLogin() == null) {
			result.redirectTo(LoginController.class).login();
		}
		
		result.include("artefatos", service.artefato());
		result.include("metas", service.meta());	
		result.include("artefato", service.artefatoAtributos(simulacao));

	}
}
