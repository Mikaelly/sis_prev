/***
 * Copyright (c) 2009 Caelum - www.caelum.com.br/opensource
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.sis.prev.controller;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.sis.prev.model.Login;
import br.com.sis.prev.service.LoginService;
import br.com.sis.prev.service.UsuarioLogadoService;

@Resource
public class LoginController {

	@Autowired
	private LoginService dao;
	
	@Autowired
	private UsuarioLogadoService usuarioLogado;
	
	private final Result result;
	
	private Validator validator;

	public LoginController(Result result, Validator validator) {
		this.result = result;
		this.validator = validator;
	}

	@Path("/")
	public void login() {
	}

	@Post
	@Path("/autentica")
	public void autentica(final Login login) {	
		validator.checking(new Validations() {
			{
				that(dao.autentica(login.getLogin(), login.getSenha()) != null , "error", "usuario.nao.encontrado");
			}
		});

		validator.onErrorUsePageOf(this).login();

		result.redirectTo(this).autoriza(login);
	}

	public void autoriza(Login login){
		usuarioLogado.sessionLogin(login, Calendar.getInstance());
		
		result.redirectTo(IndexController.class).index();
	}
}
