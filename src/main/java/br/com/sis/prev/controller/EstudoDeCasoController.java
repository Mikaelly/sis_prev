package br.com.sis.prev.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.sis.prev.model.EstudoDeCaso;
import br.com.sis.prev.service.ArtefatoService;
import br.com.sis.prev.service.EstudoDeCasoService;
import br.com.sis.prev.service.UsuarioLogadoService;

@Path("/estudoDeCaso")
@Resource
public class EstudoDeCasoController {
	
	private final Result result;
	private Validator validator;
	
	@Autowired
	private UsuarioLogadoService usuarioLogado;
	
	@Autowired
	private EstudoDeCasoService dao;
	
	@Autowired
	private ArtefatoService artefatoDao;
	

	public EstudoDeCasoController(Result result, Validator validator) {
		this.result = result;
		this.validator = validator;
	}
	
	@Path("/cadastro")
	public List<EstudoDeCaso> cadastro(){
		if(usuarioLogado.getLogin() == null) {
			result.redirectTo(LoginController.class).login();
		}
		
		result.include("artefatos", artefatoDao.select());
		return dao.select();
	}
	
	@Post("/insert")
	public void insert(final EstudoDeCaso estudoDeCaso) {
		if (usuarioLogado.getLogin() != null) {
			validator.checking(new Validations() {
				{
					that(estudoDeCaso.getDescricao() != null, "ERROR", "campos.nao.informado");
					that(estudoDeCaso.getQuantHrDiario() != 0, "ERROR", "campos.nao.informado");
					that(estudoDeCaso.getTotalHrMensal() != 0, "ERROR", "campos.nao.informado");
					that(estudoDeCaso.getInformacaoAdicional() != null, "ERROR", "campos.nao.informado");
					that(estudoDeCaso.getIdArtefato() != null, "ERROR", "campos.nao.informado");
				}
			});

			validator.onErrorForwardTo(this).cadastro();

			dao.insert(estudoDeCaso);
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
	
	@Path("/delete/{estudoDeCaso.estudoDeCaso}")
	public void delete(EstudoDeCaso estudoDeCaso) {
		if(usuarioLogado.getLogin() != null) {
			dao.delete(estudoDeCaso.getEstudoDeCaso());
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}	
	}
	
	@Path("/alterar/{estudoDeCaso.estudoDeCaso}")
	public void alterar(EstudoDeCaso estudoDeCaso) {
		if(usuarioLogado.getLogin() != null) {
			result.include("artefatos", artefatoDao.select());
			result.include("estudoDeCaso", dao.alterar(estudoDeCaso.getEstudoDeCaso()));	
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
	
	@Post
	@Path("/update/{estudoDeCaso.estudoDeCaso}")
	public void update(final EstudoDeCaso estudoDeCaso) {
		if (usuarioLogado.getLogin() != null) {
			validator.checking(new Validations() {
				{
					that(estudoDeCaso.getDescricao() != null, "ERROR", "campos.nao.informado");
					that(estudoDeCaso.getQuantHrDiario() != 0, "ERROR", "campos.nao.informado");
					that(estudoDeCaso.getTotalHrMensal() != 0, "ERROR", "campos.nao.informado");
					that(estudoDeCaso.getInformacaoAdicional() != null, "ERROR", "campos.nao.informado");
					that(estudoDeCaso.getIdArtefato() != null, "ERROR", "campos.nao.informado");
				}
			});

			validator.onErrorUsePageOf(this).alterar(estudoDeCaso);

			dao.update(estudoDeCaso);
			result.redirectTo(this).cadastro();
		} else {
			result.redirectTo(LoginController.class).login();
		}
	}
}
