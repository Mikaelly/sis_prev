package br.com.sis.prev.controller;

import java.util.ResourceBundle;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;
import br.com.caelum.vraptor.ioc.ApplicationScoped;

@Convert(LocalDate.class)
@ApplicationScoped
public class DateTimeConverter implements Converter<LocalDate> {

	@Override
	public LocalDate convert(String value, Class<? extends LocalDate> type, ResourceBundle bundle) {
		// TODO Auto-generated method stub
		try{
			final DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy"); 		
			final LocalDate dt = dtf.parseLocalDate(value);
			
			return dt;			
		} catch(Exception e) {
			e.printStackTrace();
		}		
		return null;
	}

}
